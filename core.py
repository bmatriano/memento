import numpy
import math
import matplotlib
from random import uniform


class ANN(object):
# Object Attributes
    weights = []
    modes = []
    error = []
    topography = []
# End Object Attributes
# Main Functions
    def __init__(self, topography, modes, weight=None):
        if weight == None:
            self.weights = self.weights_init(topography)
            self.topography = topography
        else:
            self.weights = weight
        self.modes = modes
    def weights_init(self, topography):
        results = []
        i = 1
        while i < len(topography):
            # Define rows
            j = 0
            results.append([])
            while j < topography[i - 1]:
                results[-1].append([])
                # Define columns
                k = 0
                while k < topography[i]:
                    results[-1][-1].append(uniform(0.0000001, 0.9999999))
                    k = k + 1
                j = j + 1
            i = i + 1
        for x in range(0, len(results)):
            results[x] = numpy.mat(results[x])
        return results
    def feed_forward_display(self, modes, feed, weights):
        output = []
        # x is weights and modes index
        for x in range(0, len(weights)):
            activation_values = feed * weights[x]
            activation_values = activation_values.tolist()
            activation_values = activation_values[0]
            # y loops through the matrix multiplied values
            for y in range(0, len(activation_values)):
                # Extend if needed
                if modes[x] == 'relu':
                    if activation_values[y] < 0:
                        activation_values[y] = 0
                if modes[x] == 'sigmoid':
                    activation_values[y] = (1 / (1 + math.exp(-activation_values[y])))
            output.append(activation_values)
            feed = numpy.mat(activation_values)
        return output
    def classify(self, feed):
        weights = self.weights
        modes = self.modes

        output = []
        # x is weights and modes index
        for x in range(0, len(weights)):
            activation_values = feed * weights[x]
            activation_values = activation_values.tolist()
            activation_values = activation_values[0]
            # y loops through the matrix multiplied values
            for y in range(0, len(activation_values)):
                # Extend if needed
                if modes[x] == 'relu':
                    if activation_values[y] < 0:
                        activation_values[y] = 0
                if modes[x] == 'sigmoid':
                    activation_values[y] = (1 / (1 + math.exp(-activation_values[y])))
            output.append(activation_values)
            feed = numpy.mat(activation_values)
        return output
    def feed_forward(self, modes, feed, weights):
        output = []
        # x is weights and modes index
        for x in range(0, len(weights)):
            activation_values = feed * weights[x]
            activation_values = activation_values.tolist()
            activation_values = activation_values[0]
            # y loops through the matrix multiplied values
            for y in range(0, len(activation_values)):
                # Extend if needed
                if modes[x] == 'relu':
                    if activation_values[y] < 0:
                        activation_values[y] = 0
                if modes[x] == 'sigmoid':
                    activation_values[y] = (1 / (1 + math.exp(-activation_values[y])))
            output.append(activation_values)
            feed = numpy.mat(activation_values)
        return output
        # Sample output:
        # Activation values + input at start
        # [[1,0,1],[0.9],[0.2,0.1,0.22]]
    def back_propagation(self, modes, y, feed, results, weights):
        net_error = 0
        error = []
        gradients = []
        deltas_w = []
        results.insert(0, feed)
        # Error computation loop
        # Output to Hidden
        for x in range(0, len(results[-1])):
            net_error = net_error + (y[x] - results[-1][x]) ** 2
            error.append((y[x] - results[-1][x]) ** 2)
        self.error.append(net_error)
        # Output to Hidden Correction
        for x in range(0, len(error)):
            if modes[-1] == 'sigmoid':
                gradients.append(error[x] * self.single_sigmoid_derivative(results[-1][x]))
            elif modes[-1] == 'relu':
                gradients.append(error[x] * self.single_relu_derivative(results[-1][x]))
        deltas_w.append((numpy.mat(gradients).T * numpy.mat(results[len(results) - 2])).T)
        # Hidden to Hidden to Input
        for z in range(len(weights) - 2, -1, -1):
            # results includes the input
            # x is for the current index of the weights
            # Weights previously adjusted; for the layer after this chronologically
            Wp = weights[z + 1]
            z_prime = []
            if self.modes[z] == 'relu':
                for x in range(0, len(results[z + 1])):
                    z_prime.append(self.single_relu_derivative(results[z + 1][x]))
            elif self.modes[z] == 'sigmoid':
                for x in range(0, len(results[z + 1])):
                    z_prime.append(self.single_relu_derivative(results[z + 1][x]))
            gradients = (numpy.mat(gradients) * numpy.mat(Wp).T)
            deltas_w.append(numpy.mat(results[z]).T * gradients)
        return list(reversed(deltas_w))
    def back_propagation_batch(self, modes, y, feed, results, weights,average_error):
        net_error = 0
        error = average_error
        gradients = []
        deltas_w = []
        results.insert(0, feed)
        # Output to Hidden
        for x in range(0, len(error)):
            if modes[-1] == 'sigmoid':
                gradients.append(error[x] * self.single_sigmoid_derivative(results[-1][x]))
            elif modes[-1] == 'relu':
                gradients.append(error[x] * self.single_relu_derivative(results[-1][x]))
        deltas_w.append((numpy.mat(gradients).T * numpy.mat(results[len(results) - 2])).T)
        # Hidden to Hidden to Input
        for z in range(len(weights) - 2, -1, -1):
            # results includes the input
            # x is for the current index of the weights
            # Weights previously adjusted; for the layer after this chronologically
            Wp = weights[z + 1]
            z_prime = []
            if self.modes[z] == 'relu':
                for x in range(0, len(results[z + 1])):
                    z_prime.append(self.single_relu_derivative(results[z + 1][x]))
            elif self.modes[z] == 'sigmoid':
                for x in range(0, len(results[z + 1])):
                    z_prime.append(self.single_relu_derivative(results[z + 1][x]))
            gradients = (numpy.mat(gradients) * numpy.mat(Wp).T)
            deltas_w.append(numpy.mat(results[z]).T * gradients)
        return list(reversed(deltas_w))
    def train(self, iterations, feed, y):
        for x in range(0, iterations):
            results = self.feed_forward(self.modes, feed, self.weights)
            delta_weights = self.back_propagation(self.modes, y, feed, results, self.weights)
            if (x > 1)  & (self.error[-1] > self.error[len(self.error) - 2]):
                continue
            self.update_weights(delta_weights)
    def error_mean(self):
        return numpy.mean(self.error)

    def clear_error(self):
        self.error = []
# Utilities Group
# End Main Functions
# Utilities Group
    def relu_derivatives(self, values):
        for x in range(0, len(values)):
            if values[x] < 0:
                values[x] = 0
            else:
                values[x] = 1
        return values
        # Returns [1,1,0,0] for example
    def sigmoid_derivative(self, values):
        for x in range(0, len(values)):
            values[x] = values[x] * (1 - values[x])
        return values
        # Returns [1.2,2.1,0.9,0.8] for example
    def single_sigmoid_derivative(self, value):
        return value * (1 - value)
    def single_relu_derivative(self, value):
        if value > 0:
            return 1
        else:
            return 0
    def error_compute(self, y, results):
        for x in range(0, len(results[-1])):
            net_error = net_error + (y[x] - results[-1][x]) ** 2
        self.error.append(net_error)
# End Utilities Group
# Attribute Updates Standalone
    def update_weights(self, delta):
        curr = numpy.array(self.weights)
        change = numpy.array(delta)
        r = curr - change
        self.weights = r
    def clear_error(self):
        self.error = []
    def change_topology(self,new_topography,new_modes):
        self.topography = new_topography
        self.weights = self.weights_init(self.topography)
        self.modes =  new_modes
# End Attr. Updates Standalone
    def network_settings(self):
        print('Weights \n',self.weights)
        print('Topography \n',self.topography)
        print('Activations \n',self.modes)

#
# x = ANN([3,1,3],['relu','sigmoid'])
# print(x.weights)
# x.train(3,[0.3,0.2,0.1],[1,0,1])
# print(x.error_mean())
# x.clear_error()
# x.train(3,[1,0,1],[0,1,0])
# print(x.error_mean())
# x.clear_error()